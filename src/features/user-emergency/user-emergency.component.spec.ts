import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserEmergencyComponent } from './user-emergency.component';

describe('UserEmergencyComponent', () => {
  let component: UserEmergencyComponent;
  let fixture: ComponentFixture<UserEmergencyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserEmergencyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UserEmergencyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
