import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IGroceryItem } from 'src/interfaces/grocery-item';
import { IRole } from 'src/interfaces/role';
import { IUser } from 'src/interfaces/user';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  heiHomesUrl = 'https://178.128.62.23/api';
  constructor(private http: HttpClient) { }

  // start of grocery CRUD
  getGroceryItems (): Observable<any> {
    return this.http.get<any>(`${this.heiHomesUrl}/grocery-items`);
  }

  newGroceryItem (groceryItem): Observable<any> {
    return this.http.post<any>(`${this.heiHomesUrl}/grocery-items`, groceryItem);
  }

  updateGroceryItem (groceryItemId, groceryItem): Observable<any> {
    return this.http.patch<any>(`${this.heiHomesUrl}/grocery-items/${groceryItemId}`, groceryItem);
  }

  deleteGroceryItem (groceryItemId): Observable<any> {
    return this.http.delete<any>(`${this.heiHomesUrl}/grocery-items/${groceryItemId}`);
  }


  // end of grocery CRUD

  // start of repait CRUD

  getRepairTypes (): Observable<any> {
    return this.http.get<any>(`${this.heiHomesUrl}/repair-types`);
  }

  newRepairType (repairType): Observable<any> {
    return this.http.post<any>(`${this.heiHomesUrl}/repair-types`, repairType);
  }

  updateRepairType (repairTypeId, repairType): Observable<any> {
    return this.http.patch<any>(`${this.heiHomesUrl}/repair-types/${repairTypeId}`, repairType);
  }
  
  deleteRepairType (repairTypeId): Observable<any> {
    return this.http.delete<any>(`${this.heiHomesUrl}/repair-types/${repairTypeId}`);
  }

  // end of repair CRUD

  // start of emergency CRUD

  getEmergencyTypes (): Observable<any> {
    return this.http.get<any>(`${this.heiHomesUrl}/emergencies`);
  }

  newEmergencyType (emergencyType): Observable<any> {
    return this.http.post<any>(`${this.heiHomesUrl}/emergencies`, emergencyType);
  }

  updateEmergencyType (emergencyTypeId, emergencyType): Observable<any> {
    return this.http.patch<any>(`${this.heiHomesUrl}/emergencies/${emergencyTypeId}`, emergencyType);
  }
  
  deleteEmergencyType (emergencyTypeId): Observable<any> {
    return this.http.delete<any>(`${this.heiHomesUrl}/emergencies/${emergencyTypeId}`);
  }

  // end of emergency CRUD

  // start of request CRUD
  getRequestTypes (): Observable<any> {
    return this.http.get<any>(`${this.heiHomesUrl}/requests`);
  }

  newRequestType (requestType): Observable<any> {
    return this.http.post<any>(`${this.heiHomesUrl}/requests`, requestType);
  }

  updateRequestType (requestTypeId, requestType): Observable<any> {
    return this.http.patch<any>(`${this.heiHomesUrl}/requests/${requestTypeId}`, requestType);
  }
  
  deleteRequestType (requestTypeId): Observable<any> {
    return this.http.delete<any>(`${this.heiHomesUrl}/requests/${requestTypeId}`);
  }

  // end of request CRUD

  // start of user CRUD

  getUsers (): Observable<IUser> {
    return this.http.get<IUser>(`${this.heiHomesUrl}/users`);
  }

  newUser (user): Observable<IUser> {
    return this.http.post<IUser>(`${this.heiHomesUrl}/user`, user);
  }

  updateUser (userId, user): Observable<IUser> {
    return this.http.post<IUser>(`${this.heiHomesUrl}/user/${userId}`, user);
  }

  deleteUser (userId): Observable<any> {
    return this.http.delete<any>(`${this.heiHomesUrl}/user/${userId}`);
  }

  // end of role CRUD 

  getRoles (): Observable<IRole> {
    return this.http.get<IRole>(`${this.heiHomesUrl}/roles`);
  }

  newRole (role): Observable<IRole> {
    return this.http.post<IRole>(`${this.heiHomesUrl}/roles`, role);
  }

  updateRole (roleId, role): Observable<IRole> {
    return this.http.patch<IRole>(`${this.heiHomesUrl}/roles/${roleId}`, role);
  }

  deleteRole (roleId): Observable<any> {
    return this.http.delete<any>(`${this.heiHomesUrl}/roles/${roleId}`);
  }
}
