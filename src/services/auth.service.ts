import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  heiHomesUrl = 'https://178.128.62.23/api';
  constructor(private http: HttpClient) { }

  login(loginCreds): Observable<any>  {
    return this.http.post<any>(`${this.heiHomesUrl}/login`, loginCreds);
  }

  isAuthenticated(): boolean {
    return true;
  }

  hasRole(loginCreds): boolean {
    return true;
  }

  
}
