import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LoginComponent } from 'src/login/login.component';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HomeComponent } from 'src/home/home.component';
import { MatRippleModule } from '@angular/material/core';
import { MatListModule } from '@angular/material/list';
import { MatTableModule } from '@angular/material/table';
import { SharedService } from 'src/services/shared.service';
import { HttpClientModule } from '@angular/common/http';
import {MatTabsModule} from '@angular/material/tabs';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { AdminEmergencyComponent } from 'src/admin-pages/admin-emergency/admin-emergency.component';
import { AdminGroceriesComponent } from 'src/admin-pages/admin-groceries/admin-groceries.component';
import { AdminRepairComponent } from 'src/admin-pages/admin-repair/admin-repair.component';
import { AdminRequestComponent } from 'src/admin-pages/admin-request/admin-request.component';
import {MatMenuModule} from '@angular/material/menu';
import {MatDialogModule} from '@angular/material/dialog';
import { GroceryItemFormComponent } from 'src/admin-pages/admin-groceries/grocery-item-form/grocery-item-form.component';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { RepairTypeFormComponent } from 'src/admin-pages/admin-repair/repair-type-form/repair-type-form.component';
import { RequestTypeFormComponent } from 'src/admin-pages/admin-request/request-type-form/request-type-form.component';
import { EmergencyTypeFormComponent } from 'src/admin-pages/admin-emergency/emergency-type-form/emergency-type-form.component';
import { AdminUserComponent } from 'src/admin-pages/admin-user/admin-user.component';
import { UserFormComponent } from 'src/admin-pages/admin-user/user-form/user-form.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AdminEmergencyComponent,
    AdminGroceriesComponent,
    AdminRepairComponent,
    AdminRequestComponent,
    AdminUserComponent,
    GroceryItemFormComponent,
    RepairTypeFormComponent,
    RequestTypeFormComponent,
    EmergencyTypeFormComponent,
    UserFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatIconModule,
    MatToolbarModule,
    MatRippleModule,
    MatListModule,
    MatTableModule,
    HttpClientModule,
    MatTabsModule,
    MatSortModule,
    MatPaginatorModule,
    MatMenuModule,
    MatDialogModule,
    NgxChartsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
