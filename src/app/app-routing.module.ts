import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminEmergencyComponent } from 'src/admin-pages/admin-emergency/admin-emergency.component';
import { AdminGroceriesComponent } from 'src/admin-pages/admin-groceries/admin-groceries.component';
import { AdminRepairComponent } from 'src/admin-pages/admin-repair/admin-repair.component';
import { AdminRequestComponent } from 'src/admin-pages/admin-request/admin-request.component';
import { AdminUserComponent } from 'src/admin-pages/admin-user/admin-user.component';
import { HomeComponent } from 'src/home/home.component';
import { LoginComponent } from 'src/login/login.component';


const routes: Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', component: HomeComponent },
  { path: 'admin-repairs', component: AdminRepairComponent },
  { path: 'admin-emergencies', component: AdminEmergencyComponent },
  { path: 'admin-groceries', component: AdminGroceriesComponent },
  { path: 'admin-requests', component: AdminRequestComponent },
  { path: 'admin-users', component: AdminUserComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
