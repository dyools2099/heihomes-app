import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroceryItemFormComponent } from './grocery-item-form.component';

describe('GroceryItemFormComponent', () => {
  let component: GroceryItemFormComponent;
  let fixture: ComponentFixture<GroceryItemFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GroceryItemFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroceryItemFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
