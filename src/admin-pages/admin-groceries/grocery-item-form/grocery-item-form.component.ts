import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-grocery-item-form',
  templateUrl: './grocery-item-form.component.html',
  styleUrls: ['./grocery-item-form.component.scss']
})
export class GroceryItemFormComponent implements OnInit {
  item = {
    id: 0,
    name: '',
    type: ''
  }

  isEditing = false;

  constructor(
    private _sharedService: SharedService,
    public dialogRef: MatDialogRef<GroceryItemFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { id: number, name: string, type: string }
  ) { }

  ngOnInit(): void {
    if (this.data) {
      this.item = this.data;
      this.isEditing = true;
    }
  }

  saveGroceryItem() {
    if (this.isEditing) {

      this._sharedService.updateGroceryItem(this.item.id, { name: this.item.name, type: this.item.type }).subscribe(data => {
        if (data.status === 200) {
          this.dialogRef.close();
        } else {
          alert('Something went wrong while processing the request. Please try again.')
        }
      });
    } else {
      this._sharedService.newGroceryItem(this.item).subscribe(data => {
        if (data.status === 201) {
          this.dialogRef.close();
        } else {
          alert('Something went wrong while processing the request. Please try again.')
        }
      });
    }

  }

}
