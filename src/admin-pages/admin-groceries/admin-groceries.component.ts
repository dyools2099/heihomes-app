import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';
import { GroceryItemFormComponent } from './grocery-item-form/grocery-item-form.component';

@Component({
  selector: 'app-admin-groceries',
  templateUrl: './admin-groceries.component.html',
  styleUrls: ['./admin-groceries.component.scss']
})
export class AdminGroceriesComponent implements OnInit {
  displayedColumns: string[] = [ 'name', 'type', 'action'];
  groceryItems : any =  [];

  constructor(
    private _sharedService: SharedService,
    public dialog: MatDialog
    ) { }

  ngOnInit(): void {
    this.getGroceryItemsList();
  }

  getGroceryItemsList() {
    this._sharedService.getGroceryItems()
    .subscribe( data => {
      if (data.content) {
        this.groceryItems = data.content.data;
      }
    });
  }

  newGroceryItem() {
    const dialogRef = this.dialog.open(GroceryItemFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getGroceryItemsList();
    });
  }

  editItem(item) {
    const dialogRef = this.dialog.open(GroceryItemFormComponent, {
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getGroceryItemsList();
    });
  }

  deleteItem(itemId) {
    this._sharedService.deleteGroceryItem(itemId).subscribe(
      res => {
        if(res.status === 200) {
          alert("Item deleted");
          this.getGroceryItemsList();
        }
      }
    );
  }
}
