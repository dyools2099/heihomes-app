import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminGroceriesComponent } from './admin-groceries.component';

describe('AdminGroceriesComponent', () => {
  let component: AdminGroceriesComponent;
  let fixture: ComponentFixture<AdminGroceriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminGroceriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminGroceriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
