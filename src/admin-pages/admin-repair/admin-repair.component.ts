import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';
import { RepairTypeFormComponent } from './repair-type-form/repair-type-form.component';

@Component({
  selector: 'app-admin-repair',
  templateUrl: './admin-repair.component.html',
  styleUrls: ['./admin-repair.component.scss']
})
export class AdminRepairComponent implements OnInit {

  displayedColumns: string[] = ['name', 'price', 'action'];
  repairTypes : any =  [];

  constructor(private _sharedService: SharedService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getRepairTypesList();
  }
  getRepairTypesList() {
    this._sharedService.getRepairTypes()
    .subscribe( data => {
      if (data.content) {
        this.repairTypes = data.content.data;
      }
    });
  }

  newRepairType() {
    const dialogRef = this.dialog.open(RepairTypeFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getRepairTypesList();
    });
  }

  editItem(item) {
    const dialogRef = this.dialog.open(RepairTypeFormComponent, {
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getRepairTypesList();
    });
  }

  deleteItem(itemId) {
    this._sharedService.deleteRepairType(itemId).subscribe(
      res => {
        if(res.status === 200) {
          alert("Item deleted");
          this.getRepairTypesList();
        }
      }
    );
  }

}
