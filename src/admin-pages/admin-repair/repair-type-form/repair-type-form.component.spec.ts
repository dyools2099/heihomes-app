import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RepairTypeFormComponent } from './repair-type-form.component';

describe('RepairTypeFormComponent', () => {
  let component: RepairTypeFormComponent;
  let fixture: ComponentFixture<RepairTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RepairTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RepairTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
