import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-repair-type-form',
  templateUrl: './repair-type-form.component.html',
  styleUrls: ['./repair-type-form.component.scss']
})
export class RepairTypeFormComponent implements OnInit {
  
  item = {
    id: 0,
    name: '',
    price_range: 0
  }

  isEditing = false;
  constructor(
    private _sharedService: SharedService,
    public dialogRef: MatDialogRef<RepairTypeFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { id: number, name: string, price_range: number }
  ) { }

  ngOnInit(): void {
    if (this.data) {
      this.item = this.data;
      this.isEditing = true;
    }
  }
  saveRepairType() {
    if (this.isEditing) {
      this._sharedService.updateRepairType(this.item.id, { name: this.item.name, price_range: this.item.price_range }).subscribe(data => {
        if (data.status === 200) {
          this.dialogRef.close();
        } else {
          alert('Something went wrong while processing the request. Please try again.')
        }
      });
    } else {
      this._sharedService.newRepairType(this.item).subscribe(data => {
        if (data.status === 201) {
          this.dialogRef.close();
        } else {
          alert('Something went wrong while processing the request. Please try again.')
        }
      });
    }

  }

}
