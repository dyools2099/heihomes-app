import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';
import { RequestTypeFormComponent } from './request-type-form/request-type-form.component';

@Component({
  selector: 'app-admin-request',
  templateUrl: './admin-request.component.html',
  styleUrls: ['./admin-request.component.scss']
})
export class AdminRequestComponent implements OnInit {
  displayedColumns: string[] = ['name', 'type', 'action'];
  requestTypes : any =  [];
  constructor(private _sharedService: SharedService,
    public dialog: MatDialog) { }
  
  ngOnInit(): void {
    this.getRequestTypesList();
  }

  getRequestTypesList() {
    this._sharedService.getRequestTypes()
    .subscribe( data => {
      if (data.content) {
        this.requestTypes = data.content.data;
      }
    });
  }

  newRequestType() {
    const dialogRef = this.dialog.open(RequestTypeFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getRequestTypesList();
    });
  }

  updateRequestType(item) {
    const dialogRef = this.dialog.open(RequestTypeFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getRequestTypesList();
    });
  }

  deleteItem(itemId) {
    this._sharedService.deleteRequestType(itemId).subscribe(
      res => {
        if(res.status === 200) {
          alert("Item deleted");
          this.getRequestTypesList();
        }
      }
    );
  }
}
