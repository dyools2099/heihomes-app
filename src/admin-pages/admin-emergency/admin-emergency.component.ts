import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';
import { EmergencyTypeFormComponent } from './emergency-type-form/emergency-type-form.component';

@Component({
  selector: 'app-admin-emergency',
  templateUrl: './admin-emergency.component.html',
  styleUrls: ['./admin-emergency.component.scss']
})
export class AdminEmergencyComponent implements OnInit {
  displayedColumns: string[] = ['name', 'type', 'action'];
  
  emergencyTypes : any =  [];
  constructor(
    private _sharedService: SharedService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.getEmergencyTypesList();
  }

  getEmergencyTypesList() {
    this._sharedService.getEmergencyTypes()
    .subscribe( data => {
      if (data.content) {
        this.emergencyTypes = data.content.data; 
      }
    });
  }

  newEmergencyType() {
    const dialogRef = this.dialog.open(EmergencyTypeFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getEmergencyTypesList();
    });
  }

  updateEmergencyType(item) {
    const dialogRef = this.dialog.open(EmergencyTypeFormComponent);

    dialogRef.afterClosed().subscribe(result => {
      this.getEmergencyTypesList();
    });
  }

  deleteItem(itemId) {
    this._sharedService.deleteEmergencyType(itemId).subscribe(
      res => {
        if(res.status === 200) {
          alert("Item deleted");
          this.getEmergencyTypesList();
        }
      }
    );
  }

}
