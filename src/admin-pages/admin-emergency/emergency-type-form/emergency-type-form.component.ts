import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SharedService } from 'src/services/shared.service';

@Component({
  selector: 'app-emergency-type-form',
  templateUrl: './emergency-type-form.component.html',
  styleUrls: ['./emergency-type-form.component.scss']
})
export class EmergencyTypeFormComponent implements OnInit {
  item = {
    id: 0,
    name: '',
    type: ''
  }

  isEditing = false;
  constructor(
    private _sharedService: SharedService,
    public dialogRef: MatDialogRef<EmergencyTypeFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { id: number, name: string, type: string }
    ) { }

  ngOnInit(): void {
    if (this.data) {
      this.item = this.data;
      this.isEditing = true;
    }
  }

  saveEmergencyType() {
    if (this.isEditing) {

      this._sharedService.updateEmergencyType(this.item.id, { name: this.item.name, type: this.item.type }).subscribe(data => {
        if (data.status === 200) {
          this.dialogRef.close();
        } else {
          alert('Something went wrong while processing the request. Please try again.')
        }
      });
    } else {
      this._sharedService.newEmergencyType(this.item).subscribe(data => {
        if (data.status === 201) {
          this.dialogRef.close();
        } else {
          alert('Something went wrong while processing the request. Please try again.')
        }
      });
    }

  }

}
