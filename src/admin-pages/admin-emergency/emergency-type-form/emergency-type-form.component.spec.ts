import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyTypeFormComponent } from './emergency-type-form.component';

describe('EmergencyTypeFormComponent', () => {
  let component: EmergencyTypeFormComponent;
  let fixture: ComponentFixture<EmergencyTypeFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmergencyTypeFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyTypeFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
