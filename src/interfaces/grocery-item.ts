export interface IGroceryItem {
    id: number,
    name: string,
    type: string
}