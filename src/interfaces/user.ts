export interface IUser {
    id: number,
    phone: string,
    password: string,
    name: string,
    email: string
}