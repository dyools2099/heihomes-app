import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/services/auth.service';



@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginCredentials = {
    phone: '',
    password: ''
  }
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
  }

  login(): void {
    console.log(this.loginCredentials)
    // this.authService.login(this.loginCredentials).subscribe(res => {
    //   if (res.status === 200) {
        this.router.navigate(['/home']);
    //   } 
    //   else {
    //     alert("Wrong phone number or password!")
    //   }
    // })
  }

}
